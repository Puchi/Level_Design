@echo OFF

SET WADCONFIGFILE=-wadcfgfile "D:\Stuff\workingon\mapping\wad.cfg"
SET TOOLS_PATH="E:\Spiele\Steam\SteamApps\common\Sven Co-op SDK\mapping\compilers"


SET /P MAPNAME=Map to compile (without extension): 
SET /P WADCONFIG=Wad Config: 

SET MAP_PATH="D:\Stuff\workingon\MAPPING\maps"

CHOICE /N /C:12 /M "1) Sven Coop Base Content || 2) Sven Co-op Addon Folder" %1
IF %ERRORLEVEL%==1 SET GAME_PATH="E:\Spiele\Steam\SteamApps\common\Sven Co-op\svencoop\maps"
IF %ERRORLEVEL%==2 SET GAME_PATH="E:\Spiele\Steam\SteamApps\common\Sven Co-op\svencoop_addon\maps"

CHOICE /N /C:QF /M "Quick or Full compile? (Q/F)"
IF %ERRORLEVEL%==1 GOTO FAST
IF %ERRORLEVEL%==2 GOTO FINAL


:FAST
SET ALL_PARAMS=-estimate -threads 3 -texdata 12000 -noinfo -chart
SET CSG_PARAMS=-clipeconomy %WADCONFIGFILE% -wadconfig %WADCONFIG% 
SET BSP_PARAMS=-nobrinks
SET VIS_PARAMS=-fast
SET RAD_PARAMS=-bounce 0 -waddir "E:\Spiele\Steam\SteamApps\common\Sven Co-op\svencoop" 
GOTO START

:FINAL
SET ALL_PARAMS=-estimate -threads 3 -texdata 12000 -noinfo -chart
SET CSG_PARAMS=-clipeconomy %WADCONFIGFILE% -wadconfig %WADCONFIG% 
SET BSP_PARAMS=-nobrink
SET VIS_PARAMS=-full
SET RAD_PARAMS=-bounce 5 -extra -waddir "E:\Spiele\Steam\SteamApps\common\Sven Co-op\svencoop" 

:START

%TOOLS_PATH%\SC-CSG.exe  %MAP_PATH%\%MAPNAME% %ALL_PARAMS% %CSG_PARAMS%
%TOOLS_PATH%\SC-BSP.exe  %MAP_PATH%\%MAPNAME% %ALL_PARAMS% %BSP_PARAMS%
%TOOLS_PATH%\SC-VIS.exe  %MAP_PATH%\%MAPNAME% %ALL_PARAMS% %VIS_PARAMS%
%TOOLS_PATH%\SC-RAD.exe  %MAP_PATH%\%MAPNAME% %ALL_PARAMS% %RAD_PARAMS%


@echo ON
copy %MAP_PATH%\%MAPNAME%.bsp %GAME_PATH% 
del %MAP_PATH%\%mapname%.max
del %MAP_PATH%\%mapname%.ext
del %MAP_PATH%\%mapname%.wa_
del %MAP_PATH%\%mapname%.prt
del %MAP_PATH%\%mapname%.rmx
@echo OFF

@pause>nul